import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse} from '@angular/common/http';
import { UserDetails } from './user-info';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginAuthService {

  loginUrl: string = 'https://29z4xk3dxg.execute-api.ap-south-1.amazonaws.com/userLogin/login';

  constructor(private httpClient: HttpClient) { }

  authLogin(user: UserDetails): Observable<UserDetails>{
    const body = JSON.stringify(user);
    //console.log(user);
    return this.httpClient.post<UserDetails>(this.loginUrl,body)
    .pipe(
      catchError(this.handleError)
    );
  }

  handleError(error: HttpErrorResponse){
    console.log("Response error");
    return throwError(error);
    }
}
