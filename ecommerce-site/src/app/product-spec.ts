export interface ProductSpec {
    id: number;
    title: string;
    price: string;
    category: string;
    description: string;
    image: string;
    rating: {[index: string]:any}; //for declaring index of an object
}
