import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginAuthService } from '../login-auth.service';
import { AlertService } from '../alert.service';
import { UserDetails } from '../user-info'; 

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm!: FormGroup;
  //loginForm: FormGroup;
  //loginForm: any;

  user!: UserDetails;
  data!: {[index: string]:any};
  userdet!: object;
  loading: any;
  queryDetail !: string;


  constructor(
    private formBuilder: FormBuilder, 
    private router: Router, 
    private loginAuth: LoginAuthService,
    private alertService: AlertService) { }

  ngOnInit() {

    this.initializeForm();

  }

  initializeForm(): void {

    this.loginForm = this.formBuilder.group({
      useremail: ["me@example.com", [
        Validators.required,
        Validators.email,
      ]],
      pswd: ['', [
        Validators.required,
        //Validators.pattern('^(?=.*[0-9])(?=.*[a-zA-Z])(a-zA-Z0-9]+)$')
      ]]
    })

  }

  /* first method for getting form elements with get method */
  get useremail() {
    return this.loginForm.get('useremail');
  }
  get pswd() {
    return this.loginForm.get('pswd');
  }
  /* another method for getting form elements without get method */
  get formControls() {                                                                                                                                             
    return this.loginForm.controls;
  }
  onSubmit(): void {
    //this.user.useremail = this.formControls.useremail.value;
    //this.user.password = this.formControls.password.value;
    //this.user = Object.assign(this.user, this.loginForm.value);
    //console.log(this.user);
    this.loginAuth.authLogin(this.loginForm.value).subscribe(data1 => {
                                                        this.data = data1;
                                                        this.queryDetail = this.storeDetails(this.loginForm.value);
                                                        if (this.queryDetail === "admin") {
                                                          this.router.navigateByUrl('/admin');
                                                        }
                                                        else if (this.queryDetail === "customer") {
                                                          this.router.navigateByUrl('/customer');
                                                        }
                                                        else {
                                                          this.alertService.success(this.data?.message);
                                                        }
                                                          
                                                        //console.log(this.user);
                                                        }, 
                                                        err => {
                                                          this.data = err;
                                                          console.log("Error in POST "+this.data.error["message"]);
                                                          this.alertService.error(this.data.error["message"]);
                                                          this.loading = false;
                                                        },
                                                        () => {console.log("Observable completed")}
                                                    );
    console.log(this.user);
    //this.userdet = JSON.parse(this.data)
    //console.log(this.userdet);                                                                                                                                          

    //this.router.navigateByUrl('/admin');                                                                                                                                          
  }

  storeDetails(user: UserDetails) {
    let users = [];
    if (localStorage.getItem('Users')) {
      users = JSON.parse(localStorage.getItem('Users') || '{}');
      users = [...users, user];
    }
    else {
      users = [user];
    }
    localStorage.setItem('Users', JSON.stringify(users));
    if (user.useremail==="admin@gmail.com") {
      return "admin";
    }
    else if (user.useremail==="customer@gmail.com") {
      return "customer";
    }
    else return "success";
  }

}
