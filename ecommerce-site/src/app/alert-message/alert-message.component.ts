import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { AlertService } from '../alert.service';



@Component({
  selector: 'app-alert-message',
  templateUrl: './alert-message.component.html',
  styleUrls: ['./alert-message.component.css']
})
export class AlertMessageComponent implements OnInit, OnDestroy {
  
  private subscription!: Subscription;
  public message: any;

  constructor(private alertService: AlertService) { }

  ngOnInit() {
    this.subscription = this.alertService.getAlert()
            .subscribe(message => {
                switch (message && message.type) {
                    case 'success':
                        message.cssClass = 'alert alert-success col-md-3';
                        break;
                    case 'error':
                        message.cssClass = 'alert alert-danger col-md-3';
                        break;
                }

                this.message = message;
                console.log(this.message);
            });
   }

   ngOnDestroy() {
    this.subscription.unsubscribe();
  }
  
}
