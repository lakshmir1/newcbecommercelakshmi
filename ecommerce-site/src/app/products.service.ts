import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse} from '@angular/common/http';
import { ProductSpec } from './product-spec';
import { PRODUCTS } from './mock-products';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  productsUrl: string = 'https://fakestoreapi.com';

  constructor(private httpClient: HttpClient) { }

  getProducts(): ProductSpec[] {
    return PRODUCTS;
  }

  getProductHttp(): Observable<ProductSpec[]> {
    return this.httpClient.get<ProductSpec[]>(`${this.productsUrl}/products`)
    .pipe(
      catchError(this.handleError)
    );
  }
  getProductById(id: number): Observable<ProductSpec> {
    return this.httpClient.get<ProductSpec>(`${this.productsUrl}/products/${id}`)
    .pipe(
      catchError(this.handleError)
    );
  }
  getProductByCategory(category: string): Observable<ProductSpec> {
    return this.httpClient.get<ProductSpec>(`${this.productsUrl}/products/categeory${category}`)
    .pipe(
      catchError(this.handleError)
    );
  }
  /*
  getProductHttp(): Observable<ProductSpec[]> {
    const products = of(PRODUCTS);
    return products
  }
  */
  handleError(error: HttpErrorResponse){
    console.log("Response error");
    return throwError(error);
    }
}
