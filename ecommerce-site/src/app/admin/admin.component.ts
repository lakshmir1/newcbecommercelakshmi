import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ProductsService } from '../products.service';
import { ProductSpec } from '../product-spec';
import { NgxSpinnerService } from "ngx-spinner";
import { ExcelService } from '../services/excel.service';


@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  adminForm!: FormGroup;
  products: ProductSpec[] = [];

  constructor(private formBuilder: FormBuilder, 
    private router: Router, 
    private productsService: ProductsService,
    private spinner: NgxSpinnerService,
    private excelService: ExcelService) { }

  ngOnInit() {
    this.initializeForm();
    this.getProducts();
  }

  initializeForm(): void {
    this.adminForm = this.formBuilder.group({

    })
  }

  getProducts(): void {
    this.spinner.show();
    //this.products = this.productsService.getProducts();
    //this.productsService.getProductHttp()
    //.subscribe(products => this.products = products);
    this.productsService.getProductHttp()
    .subscribe((data: any) => {
      this.products = data;
      console.log(this.products);
      this.spinner.hide();
    });
  }

  exportAsXLSX():void {
    let excelData:any = [];
    let i = 0;
    this.products.forEach(element => {
      //excelData[i] = {id:element.id, "title":element.title, "price":element.price, "category":element.category, "description":element.description};
      excelData.push({"Product id":element.id, "Product Title":element.title, "ProductPrice":element.price, "Category":element.category, "Description":element.description});
      
      i++;
    });

    this.excelService.exportAsExcelFile(excelData, 'products_data');
  }

  logout(): void {
    //console.log(this.loginForm);
      this.router.navigateByUrl('/login');
  }

}
